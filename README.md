# archwiki downloader

[![works badge](https://cdn.jsdelivr.net/gh/nikku/works-on-my-machine@v0.2.0/badge.svg)](https://github.com/nikku/works-on-my-machine)

This little script downloads the awesome [Archwiki](https://wiki.archlinux.org) for offline use and installs `wiki-search` and `wiki-search-html` scripts provided by `arch-wiki-lite`.<br>
It is meant to be used on fedora (tested on f32) but it shouldn't be too hard to edit it for another distributions.

### purpose
As of now there are no native fedora packages doing this job, this script solves it for me.<br>
Inspiration taken from the PKGBUILDs of [arch-wiki-docs](https://git.archlinux.org/svntogit/community.git/tree/trunk?h=packages/arch-wiki-docs) and [arch-wiki-lite](https://git.archlinux.org/svntogit/community.git/tree/trunk?h=packages/arch-wiki-lite).

### deps
It comes with just a few dependencies:

* `python3`
* `python3-simplemediawiki`
* `python3-cssselect`
* `python3-lxml`
* `dialog`

...plus I used `rsync`, feel free to change it to `cp` (and update paths accordingly)

### license
[WTFPL](http://wtfpl.net)
